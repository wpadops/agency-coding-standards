#Technical Guidelines

Unless your entire creative and it's code are insulated inside of an Iframe, you must adhere to the following rules.

##JavaScript

Do not pollute the global namespace. Wrap all of your code in a closure, or, if needed, inside a single global Object. For example:

    <script>
      (function(){
        //all of your JavaScript code should go here
      })();
    </script>

Or:

    <script>
      var myGlobalNamespace = {
        someAttribute: true,
        someFunction: function(){
          //do stuff
        }
      };
    </script>

All ad executions should take special care to use carefully namespaced global variables.

###Script loading

Be conservative in script loading. Use a single minified, gzipped file (or as few as possible).
Use asynchronous loading techiniques wherever possible.

    <script src="some_url.js" async></script>

Or:

    <script>
      (function(d){
        var s = d.createElement('script'),
            target = d.body || d.getElementsByTagName('head')[0];
        s.src = 'some_url.js';
        s.async = true;
        target.appendChild(s);
      })(document);
    </script>

###jQuery

Most of our pages are already using jQuery. If your code requires jQuery, make sure to check the existance of jQuery on the page before adding it yourself. For example:

    <script>
      window.jQuery || document.write('<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"><\/script>');
    </script>

    <script>
      (function($){
        //do stuff with $
      })(window.jQuery);
    </script>

If you absolutely must use a different version of jQuery than what is provided by the page, you must call jQuery's `noConflict(true)` method to not alter the page's original `$` or `jQuery` Objects. For example:

    <script src="some_other_jQuery.js"></script>

    <script>
      (function($){

        // All your code here. You can safely use $ in this scope as reference to your
        // jQuery Object, without affecting the page's original jQuery Object

      })(jQuery.noConflict(true));

      //jQuery and $ still reference the page's original jQuery Object.

    </script>


##CSS
Do not overwrite any global CSS. All CSS selectors must descend from the containing element of your HTML. For example:

    #myLeaderboardAdContainer p {
      font-size: 12px;
    }
    #myLeaderboardAdContainer .hide{
      display: none;
    }

Expect your creative to be centered in a container `div`. Style your margins and alignments accordingly.  

Our website and it's subdomains and properties explicitly support  >= IE7, Firefox, Chrome and Safari - use of CSS3 should plan accordingly, at the very least including fallbacks for older browsers. (Directions to install Chrome Frame is not a fallback.)
